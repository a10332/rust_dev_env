# Generates an image with a rust compiler and an app that displays hello world.


# Using Ubuntu was the base image.
FROM ubuntu
# Specifying the shell for all the following RUN commands.
SHELL ["/bin/bash", "-c"]
# Let's update the distribution.
RUN apt update && apt -y dist-upgrade
# Installing vim to edit source code.
RUN apt -y install vim
# Installing build-essentials as some rust crates will need the compilers from it.
RUN apt -y install build-essential
# Same reason for libssl-dev. Note that libssl-dev is OpenSsl.
# !SPECIAL THANKS! to: https://github.com/sfackler/rust-openssl/issues/1021#issuecomment-441385845
RUN apt -y install libssl-dev
# Need to install debconf-utils to be able to pass the right settings to pkg-config.
# !SPECIAL THANKS! to: http://www.microhowto.info/howto/perform_an_unattended_installation_of_a_debian_package.html
RUN apt -y install debconf-utils && echo tzdata  tzdata/Areas    select  America | debconf-set-selections && echo tzdata  tzdata/Zones/America    select  Montreal | debconf-set-selections
# Installing pkg-config that is used by some crates that use libssl-dev.
RUN export DEBIAN_FRONTEND=noninteractive && apt-get update -q && apt-get install -q -y -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" pkg-config
# Installing curl to get rustup installer.
RUN apt -y install curl
# Getting tihe rustup installer script, making it executable and executing it. Then, removing it.
RUN curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs --output rustup-init.sh && ls && chmod +x rustup-init.sh && bash ./rustup-init.sh -y -v && rm rustup-init.sh
# Sourcing the environment variable for cargo and updating rust.
RUN source $HOME/.cargo/env && rustup --version && rustup update
# Creating a new default application named app_01.
RUN source $HOME/.cargo/env && cargo new app_01 --bin
# Building the application.
RUN source $HOME/.cargo/env && cd app_01 && cargo build
# Installing git. I will want to use this image as a base for other things.
RUN apt -y install git
# At this point, ```docker images``` and find the id of your image. Then, ```docker run --name rustdev01 -d IMAGE_ID sleep infinity```.
# Then, ```docker exec -it rustdev01 bash```. Now, you can try everything you want in that container.
# Remember that what happens in the container stays in the container! When it shuts down, you'll lose all changes. Very practical to experiment.

